# -*- coding: utf-8 -*-
"""
Created on Sat Feb 06 15:07:49 2016

@author: Mike
"""

# import potentially useful packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os
import inspect
from geopy.distance import vincenty
from mpl_toolkits.basemap import Basemap
import weather


def main():
'''
This function plots the locations of 100 commercial buildings across the United States
and the closest weather station to each building.
'''
	#--------------------------
	# Locate the data files
	#--------------------------
    # locate the meta data file
    # get the current file location

    
    # locate the building meta data
    
    # locate the weather data
    
    # locate tmy3 weather meta data       
    
	#---------------------------
	# determine the relevant weather stations
	# --------------------------
	# Write a function that returns the weather station
	# closest to each building.

    
    # plot a map of the united states using Basemap
	# Plot only the weather stations closest to the buildings.

    

if __name__ == '__main__':
    main()